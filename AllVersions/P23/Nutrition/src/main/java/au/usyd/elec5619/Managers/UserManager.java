package au.usyd.elec5619.Managers;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Doctor;
public interface UserManager extends Serializable{

	public String getLoggedInUser();
	
	public List<User> getUser(String username);

    public void addUser(User user);

    public void updateUser(User user);
    
    public void deleteUser(int id);
    
    public int getUserForLogin(String username,String password);
    
    public void logOutUser();
    
    public User returnUserObject();
    
    
	public String getLoggedInDoctor();
	
	public List<Doctor> getDoctor(String username);
	
	public List<Doctor> getAllDoctors();

    public void addDoctor(Doctor doctor);

    public void updateDoctor(Doctor doctor);
    
    public void deleteDoctor(int id);
    
    public int getDoctorForLogin(String username,String password);
    
    public void logOutDoctor();
    
    public Doctor returnDoctorObject();
    
    
    
    public List<Meal> getMeals(String username);
    
    public void addMeal(Meal meal);
    
    public void deleteMeal(int id);
}