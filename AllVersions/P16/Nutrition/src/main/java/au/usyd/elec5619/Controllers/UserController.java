package au.usyd.elec5619.Controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Managers.UserManager;


@Controller
public class UserController {
	
	int loggedin = 0;
	
	@Resource(name="userManager")
	private UserManager userManager;

	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String addUser(HttpServletRequest httpServletRequest) {
		
		User user = new User();
		user.setUserName(httpServletRequest.getParameter("username"));
		user.setPassword(httpServletRequest.getParameter("password"));
		user.setFirstName(httpServletRequest.getParameter("firstName"));
		user.setLastName(httpServletRequest.getParameter("lastName"));
		user.setDateOfBirth(httpServletRequest.getParameter("dateOfBirth"));
		user.setEmailAddress(httpServletRequest.getParameter("email"));
		user.setIllness(httpServletRequest.getParameter("illness"));
		user.setAge(httpServletRequest.getParameter("age"));
		user.setWeight(httpServletRequest.getParameter("weight"));
		user.setHeight(httpServletRequest.getParameter("height"));
		//user.setPrice(Double.valueOf(httpServletRequest.getParameter("price")));
		this.userManager.addUser(user);
		
		return "redirect:/login.htm";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView logIn(HttpServletRequest httpServletRequest) {
		
		String s1 =  httpServletRequest.getParameter("username");
		String s2 =  httpServletRequest.getParameter("password");
		
		
		loggedin = this.userManager.getUserForLogin(s1, s2);
		
		String currentUser = this.userManager.getLoggedInUser();
	    List<User> users =  this.userManager.getUser(currentUser);
        Map<String, Object> myModel1 = new HashMap<String, Object>();
        myModel1.put("users", users);
//
        if(loggedin == 1)
        {	
        	return new ModelAndView("users", "model1", myModel1);
        }
        else
        {
        	return new ModelAndView("", "", myModel1);
        }
		
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)	
	public ModelAndView users(Locale locale3, Model model3)
	{
			String currentUser = this.userManager.getLoggedInUser();
		    List<User> users =  this.userManager.getUser(currentUser);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("users", users);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("users", "model1", myModel1);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel1);
	        }
	}
	
	@RequestMapping(value = "/meals", method = RequestMethod.GET)	
	public ModelAndView meals(Locale locale, Model model)
	{
			
			String currentUser = this.userManager.getLoggedInUser();
		    List<Meal> meals =  this.userManager.getMeals(currentUser);
	        Map<String, Object> myModel = new HashMap<String, Object>();
	        myModel.put("meals", meals);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("meals", "model", myModel);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel);
	        }
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)	
	public String logout(Locale locale4, Model model4)
	{
			this.userManager.logOut();
			loggedin = 0;
	        return "redirect:/login";

	}
	
	
	
	/*
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String editProduct(@PathVariable("id") int id, Model uiModel) {
		
		User user = this.userManager.getUserById(id);
		uiModel.addAttribute("user", user);
		
		return "edit";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editProduct(@Valid User user) {
		
		this.userManager.updateUser(user);
		System.out.println(user.getId());
		
		return "redirect:/hello.htm";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String deleteUSer(@PathVariable("id") int id) {
		
		this.userManager.deleteUser(id);
		
		return "redirect:/hello.htm";
	}
	*/
}
