<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
</head>
<body>
	<div>
		<h1>Welcome Nutrition Health</h1>
		<p>To get started, you need to enter your details to register with
			us. Or login to access your details, if you are already registered.</p>
	</div>
	<a class="btn btn-primary" href="register.html">Register � </a> 
	<br/>
	<a class="btn btn-primary" href="login.html">Login � </a>
</body>
</html>