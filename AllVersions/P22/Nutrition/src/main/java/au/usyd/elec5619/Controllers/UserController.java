package au.usyd.elec5619.Controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Product;
import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Doctor;
import au.usyd.elec5619.Managers.UserManager;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);
	
	int loggedin = 0;
	
	int loggedinDoctor = 0;
	
	@Resource(name="userManager")
	private UserManager userManager;

//***************************************** BEGIN USER  *************************************************************************************************************************************************
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	String register(@RequestParam("username") String userName, @RequestParam("password") String password, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, 
			@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("email") String email, @RequestParam("illness") String illness,  
			@RequestParam("age") String age, @RequestParam("weight") String weight, @RequestParam("height") String height, 
			@RequestParam("file") MultipartFile file) 
	{
			
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/Users/";
				File dir = new File(rootPath + File.separator );
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator+ userName +".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				User user = new User();
				
				user.setUserName(userName);
				user.setPassword(password);
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setDateOfBirth(dateOfBirth);
				user.setEmailAddress(email);
				user.setIllness(illness);
				user.setAge(age);
				user.setWeight(weight);
				user.setHeight(height);
				
				
				this.userManager.addUser(user);
				
				return "redirect:/login.htm";
			

			} 
			catch (Exception e) 
			{
				return "redirect:/register.htm";
			}
		} 	
		
		else 
		{
			
			User user = new User();
			
			user.setPassword(userName);
			user.setPassword(password);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setDateOfBirth(dateOfBirth);
			user.setEmailAddress(email);
			user.setIllness(illness);
			user.setAge(age);
			user.setWeight(weight);
			user.setHeight(height);
			
			
			this.userManager.addUser(user);
			
			return "redirect:/login.htm";
		}
	}
	
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String logIn(HttpServletRequest httpServletRequest) {
		
		String s1 =  httpServletRequest.getParameter("username");
		String s2 =  httpServletRequest.getParameter("password");
		
		loggedin = this.userManager.getUserForLogin(s1, s2);

        if(loggedin == 1)
        {	
        	return "redirect:/meals.htm";
        }
        else
        {
        	return "redirect:/login.htm";
        }
		
	}

	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)	
	public ModelAndView profile(Locale locale3, Model model3)
	{
			String currentUser = this.userManager.getLoggedInUser();
		    List<User> profile =  this.userManager.getUser(currentUser);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("profile", profile);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("profile", "model1", myModel1);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel1);
	        }
	}
	

	@RequestMapping(value = "/profile", method = RequestMethod.POST)

	String updateProfile(@RequestParam("password") String password, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, 
			@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("email") String email, @RequestParam("illness") String illness,  
			@RequestParam("age") String age, @RequestParam("weight") String weight, @RequestParam("height") String height, 
			@RequestParam("file") MultipartFile file) 
	{

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/Users/";
				File dir = new File(rootPath + File.separator );
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + this.userManager.getLoggedInUser() +".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				User object = this.userManager.returnUserObject();
				
				object.setPassword(password);
				object.setFirstName(firstName);
				object.setLastName(lastName);
				object.setDateOfBirth(dateOfBirth);
				object.setEmailAddress(email);
				object.setIllness(illness);
				object.setAge(age);
				object.setWeight(weight);
				object.setHeight(height);
				
				
				this.userManager.updateUser(object);
			
				String currentUser = this.userManager.getLoggedInUser();
			    List<User> profile =  this.userManager.getUser(currentUser);
		        Map<String, Object> myModel1 = new HashMap<String, Object>();
		        myModel1.put("profile", profile);
		        
		        return "redirect:/profile.htm";
			} 
			catch (Exception e) 
			{
				return "redirect:/profile.htm";
			}
		} 	
		
		else 
		{
			
			User object = this.userManager.returnUserObject();
			
			object.setPassword(password);
			object.setFirstName(firstName);
			object.setLastName(lastName);
			object.setDateOfBirth(dateOfBirth);
			object.setEmailAddress(email);
			object.setIllness(illness);
			object.setAge(age);
			object.setWeight(weight);
			object.setHeight(height);
			
			
			this.userManager.updateUser(object);
		
			String currentUser = this.userManager.getLoggedInUser();
		    List<User> profile =  this.userManager.getUser(currentUser);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("profile", profile);
	        
	        return "redirect:/profile.htm";
		}
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)	
	public String logout(Locale locale4, Model model4)
	{
			this.userManager.logOutUser();
			loggedin = 0;
	        return "redirect:/login";

	}
	
//***************************************** END USER  *************************************************************************************************************************************************	

	
	
	
//***************************************** BEGIN DOCTOR  *************************************************************************************************************************************************
	
	
	@RequestMapping(value = "/doctorRegister", method = RequestMethod.POST)
	String doctorRegister(@RequestParam("username") String userName, @RequestParam("password") String password, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("email") String email,
			@RequestParam("about") String about,@RequestParam("file") MultipartFile file) 
	{
			
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/Doctors/";
				File dir = new File(rootPath + File.separator );
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator+ userName +".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				Doctor doctor = new Doctor();
				
				doctor.setDoctorUserName(userName);
				doctor.setPassword(password);
				doctor.setFirstName(firstName);
				doctor.setLastName(lastName);
				doctor.setDateOfBirth(dateOfBirth);
				doctor.setEmailAddress(email);
				doctor.setAbout(about);
				
				
				this.userManager.addDoctor(doctor);
				
				return "redirect:/doctorLogin.htm";
			

			} 
			catch (Exception e) 
			{
				return "redirect:/doctorLogin.htm";
			}
		} 	
		
		else 
		{
			
			Doctor doctor = new Doctor();
			
			doctor.setDoctorUserName(userName);
			doctor.setPassword(password);
			doctor.setFirstName(firstName);
			doctor.setLastName(lastName);
			doctor.setDateOfBirth(dateOfBirth);
			doctor.setEmailAddress(email);
			doctor.setAbout(about);
			
			
			this.userManager.addDoctor(doctor);
			
			return "redirect:/doctorLogin.htm";
		
		}
	}
	
	@RequestMapping(value="/doctorLogin", method=RequestMethod.POST)
	public String doctorLogin(HttpServletRequest httpServletRequest) {
		
		String s1 =  httpServletRequest.getParameter("username");
		String s2 =  httpServletRequest.getParameter("password");
		
		loggedinDoctor = this.userManager.getDoctorForLogin(s1, s2);

        if(loggedinDoctor == 1)
        {	
        	return "redirect:/doctorProfile.htm";
        }
        else
        {
        	return "redirect:/doctorLogin.htm";
        }
		
	}

	@RequestMapping(value = "/doctorProfile", method = RequestMethod.GET)	
	public ModelAndView doctorProfile(Locale locale3, Model model3)
	{
			String currentDoctor = this.userManager.getLoggedInDoctor();
		    List<Doctor> doctorProfile =  this.userManager.getDoctor(currentDoctor);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("doctorProfile", doctorProfile);
	        
	        if(loggedinDoctor == 1)
	        {	
	        	return new ModelAndView("doctorProfile", "model1", myModel1);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel1);
	        }
	}
	
	@RequestMapping(value = "/doctorProfile", method = RequestMethod.POST)

	String updateDoctorProfile(@RequestParam("password") String password, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, 
			@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("email") String email, @RequestParam("about") String about, 
			@RequestParam("file") MultipartFile file) 
	{

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/Doctors/";
				File dir = new File(rootPath + File.separator );
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + this.userManager.getLoggedInDoctor() +".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				Doctor object = this.userManager.returnDoctorObject();
				
				object.setPassword(password);
				object.setFirstName(firstName);
				object.setLastName(lastName);
				object.setDateOfBirth(dateOfBirth);
				object.setEmailAddress(email);
				object.setAbout(about);
				
				this.userManager.updateDoctor(object);
			
				String currentDoctor = this.userManager.getLoggedInDoctor();
			    List<Doctor> doctorProfile =  this.userManager.getDoctor(currentDoctor);
		        Map<String, Object> myModel1 = new HashMap<String, Object>();
		        myModel1.put("doctorProfile", doctorProfile);
		        
		        return "redirect:/doctorProfile.htm";
			} 
			catch (Exception e) 
			{
				return "redirect:/doctorProfile.htm";
			}
		} 	
		
		else 
		{
			
			Doctor object = this.userManager.returnDoctorObject();
			
			object.setPassword(password);
			object.setFirstName(firstName);
			object.setLastName(lastName);
			object.setDateOfBirth(dateOfBirth);
			object.setEmailAddress(email);
			object.setAbout(about);
			
			this.userManager.updateDoctor(object);
		
			String currentDoctor = this.userManager.getLoggedInDoctor();
		    List<Doctor> doctorProfile =  this.userManager.getDoctor(currentDoctor);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("doctorProfile", doctorProfile);
	        
	        return "redirect:/doctorProfile.htm";
		}
	}
	
	@RequestMapping(value = "/doctorLogout", method = RequestMethod.GET)	
	public String doctorLogout(Locale locale4, Model model4)
	{
			this.userManager.logOutDoctor();
			loggedinDoctor = 0;
	        return "redirect:/doctorLogin";

	}
	

//***************************************** END DOCTOR  *************************************************************************************************************************************************
	
	
	
	
//***************************************** BEGIN MEAL  *************************************************************************************************************************************************
	
	@RequestMapping(value = "/meals", method = RequestMethod.POST)

	String mealsUpload(@RequestParam("number") String number, @RequestParam("date") String date, 
			@RequestParam("description") String description, @RequestParam("file") MultipartFile file) 
	{
			
		Random rand = new Random();
		int  n = rand.nextInt(1000000) + 1;
		
		String photoNumber = String.valueOf(n);
		
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/Foods/";
				File dir = new File(rootPath + File.separator + this.userManager.getLoggedInUser());
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + photoNumber + ".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				Meal meal = new Meal();
				
				meal.setUserName(this.userManager.getLoggedInUser());
				meal.setDate(date);
				meal.setNumber(number);
				meal.setDescription(description);
				meal.setPhotoName(photoNumber);
				
				this.userManager.addMeal(meal);
				
				return "redirect:/meals.htm";
			} 
			catch (Exception e) 
			{
				return "redirect:/meals.htm";
			}
		} 	
		
		else 
		{
			
			Meal meal = new Meal();
			
			meal.setUserName(this.userManager.getLoggedInUser());
			meal.setDate(date);
			meal.setNumber(number);
			meal.setDescription(description);
			meal.setPhotoName(number+date);
			
			this.userManager.addMeal(meal);
			
			return "redirect:/meals.htm";
		}
	}
	
	@RequestMapping(value = "/meals", method = RequestMethod.GET)	
	public ModelAndView meals(Locale locale, Model model)
	{
			
			String currentUser = this.userManager.getLoggedInUser();
		    List<Meal> meals =  this.userManager.getMeals(currentUser);
	        Map<String, Object> myModel = new HashMap<String, Object>();
	        myModel.put("meals", meals);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("meals", "model", myModel);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel);
	        }
	}
	
	@RequestMapping(value="/meals/{id}", method=RequestMethod.GET)
	public String deleteMeal(@PathVariable("id") int id) {
		
		this.userManager.deleteMeal(id);
		
		return "redirect:/meals.htm";
	}

//***************************************** END MEAL  *************************************************************************************************************************************************

}
