						package au.usyd.elec5619.Managers;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Doctor;

@Service(value="userManager")
@Transactional
public class DatabaseUserManager implements UserManager {
	
	public static int loggedin = 0;
	
	public static int loggedinDoctor = 0;
	
	public static String loggedin_User = "";
	
	public static String loggedin_Doctor = "";
	
	User userObject = new User();
	
	Doctor doctorObject = new Doctor();
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf1) {
		this.sessionFactory = sf1;
	}
	
	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public void updateUser(User user) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}
	
	@Override
	public void deleteUser(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		currentSession.delete(user);
	}

	@Override
	public void addDoctor(Doctor doctor) {
		this.sessionFactory.getCurrentSession().save(doctor);
	}	
	
	@Override
	public void updateDoctor(Doctor doctor) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(doctor);
	}
	
	@Override
	public void deleteDoctor(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Doctor doctor = (Doctor) currentSession.get(Doctor.class, id);
		currentSession.delete(doctor);
	}
	
	
	@Override
	public void addMeal(Meal meal) {
		this.sessionFactory.getCurrentSession().save(meal);
	}

	@Override
	public void deleteMeal(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Meal meal = (Meal) currentSession.get(Meal.class, id);
		currentSession.delete(meal);
	}

	@Override
	public List<User> getUser(String username) {
		
		String hql = "FROM User where username=:username";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<User> user = query.list();
		return user;
		
	}
	
	public int getUserForLogin(String username,String password) {
		
		String hql = "from User where username=:username and password=:password";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("username", username);
        query.setString("password", password);
        User result = (User) query.uniqueResult();
        
        if(result != null)
        {
        	loggedin = 1;
        	loggedin_User = username;
        	userObject = result;
        }		
        
        return loggedin;
	}

	public String getLoggedInUser()
	{
		return loggedin_User;
	}
	
	public User returnUserObject()
	{
		return userObject;
	}
	
	public void logOutUser()
	{
		loggedin = 0;
	}
	
	@Override
	public List<Doctor> getDoctor(String doctorusername) {
		
		String hql = "FROM Doctor where doctorusername=:doctorusername";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("doctorusername", doctorusername);
		List<Doctor> Doctor = query.list();
		return Doctor;
	}
	
	public int getDoctorForLogin(String doctorusername,String password) {
		
		String hql = "from Doctor where doctorusername=:username and password=:password";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("username", doctorusername);
        query.setString("password", password);
        Doctor result = (Doctor) query.uniqueResult();
        
        if(result != null)
        {
        	loggedinDoctor = 1;
        	loggedin_Doctor = doctorusername;
        	doctorObject = result;
        }		
        
        return loggedinDoctor;
	}
	
	public String getLoggedInDoctor()
	{
		return loggedin_Doctor;
	}
	
	public Doctor returnDoctorObject()
	{
		return doctorObject;
	}
	
	public void logOutDoctor()
	{
		loggedinDoctor = 0;
	}
	
	public List<Meal> getMeals(String username) {
		
		String hql = "FROM Meal where username=:username";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Meal> meals = query.list();
		return meals;
		// ask how to get a single string as the result of query
	}
	
	public void deleteMeal(String id) {
		
		String hql = "DELETE FROM Meal where id=:mealId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("mealId", id);
		Meal meal = (Meal)query.uniqueResult();
	}


}
