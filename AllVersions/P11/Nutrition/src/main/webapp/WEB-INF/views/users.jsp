<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
	<head>
		<title><fmt:message key="title" /></title>
	</head>
	<body>
		<div>
			<ul>
				<li><a href="../home.htm">Home</a></li>
				<li><a href="register.htm">Register</a></li>
				<li class="active"><a href="login.htm">Login</a></li>
				<li class="active"><a href="logout.htm">LogOut</a></li>
			</ul>
		</div>
		<h1>
			<fmt:message key="heading" />
		</h1>
		<p>
			<fmt:message key="greeting" />
			<c:out value="${model1.now}" />
		</p>
		<h3>Users</h3>
		<c:forEach items="${model1.users}" var="usr">
		
			<c:out value="${usr.userName}" /> ||
			<c:out value="${usr.firstName}" /> ||
			<c:out value="${usr.dateOfBirth}" /> ||
			<c:out value="${usr.lastName}" /> ||
			<c:out value="${usr.emailAddress}" /> ||
			<a href="user/edit/${usr.id }">edit</a> ||
			<a href="user/delete/${usr.id }">delete</a> 
			<br>
			<br>
		</c:forEach>
	
		<!-- link to the increase price page -->
		<br>
		<!--  <a href="<c:url value="priceincrease.htm"/>">Increase Prices</a>  -->
		<br>
	</body>
</html>