package au.usyd.elec5619.Controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.usyd.elec5619.Managers.UserManager;

public class UserInventoryController implements Controller {
	
	protected final Log logger1 = LogFactory.getLog(getClass());

    private UserManager userManager;

    public ModelAndView handleRequest(HttpServletRequest request1, HttpServletResponse response1)
            throws ServletException, IOException {

        String now1 = (new java.util.Date()).toString();
        logger1.info("returning hello view with " + now1);

        Map<String, Object> myModel1 = new HashMap<String, Object>();
        myModel1.put("now", now1);
        myModel1.put("users", this.userManager.getUsers());

        return new ModelAndView("users", "model1", myModel1);
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
	
}
