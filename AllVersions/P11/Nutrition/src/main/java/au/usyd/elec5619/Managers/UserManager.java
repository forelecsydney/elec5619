package au.usyd.elec5619.Managers;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.Classes.User;

public interface UserManager extends Serializable{

   // public void increasePrice(int percentage);
    
    public List<User> getUsers();
    
    public void addUser(User user);
    
    public User getUserById(int id);
    
    public void updateUser(User user);
    
    public void deleteUser(int id);
    
    public int getUserForLogin(String username,String password);
    
    public void logIn();
    
    public void logOut();
}