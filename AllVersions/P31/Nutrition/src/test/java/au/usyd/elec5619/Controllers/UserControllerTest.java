package au.usyd.elec5619.Controllers;

import java.util.Map;

import junit.framework.TestCase;

import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Controllers.*;
import au.usyd.elec5619.Managers.*;

public class UserControllerTest extends TestCase {

    public void testView() throws Exception{
        SimpleUserController controller = new SimpleUserController();
        controller.setUserManager(new SimpleUserManager());
        ModelAndView modelAndView = controller.handleRequest(null, null);
        assertEquals("user", modelAndView.getViewName());
        assertNotNull(modelAndView.getModel());
        Map modelMap = (Map) modelAndView.getModel().get("model");
    }
	
}
