						package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.User;

@Service(value="userManager")
@Transactional
public class DatabaseUserManager implements UserManager {
	
	public int loggedin = 0;
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf1) {
		this.sessionFactory = sf1;
	}
	
	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public void logIn() {
		
	}
	
	@Override
	public User getUserById(int id) {
		Session currentSession1 = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession1.get(User.class, id);
		return user;
	}
	
	@Override
	public void updateUser(User user) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}
	
	
	@Override
	public void deleteUser(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		currentSession.delete(user);
	}

	/*
	@Override
	public void increasePrice(int percentage) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Product> products = currentSession.createQuery("FROM Product").list();
		
		if (products != null) {
            for (Product product : products) {
                double newPrice = product.getPrice().doubleValue() * 
                                    (100 + percentage)/100;
                product.setPrice(newPrice);
                currentSession.save(product);
            }
        }
	}
	*/
	@Override
	public List<User> getUsers() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
	}
	
	public int getUserForLogin(String username,String password) {
		
		String hql = "from User where username=:username and password=:password";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("username", username);
        query.setString("password", password);
        User result = (User) query.uniqueResult();
        
        if(result != null)
        {
        	loggedin = 1;
        }		
        
        return loggedin;
	}
}
