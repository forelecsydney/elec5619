package au.usyd.elec5619.Classes;

import junit.framework.TestCase;

public class UserTest extends TestCase {

    private User user;
    
    protected void setUp() throws Exception {
        user = new User();
    }
	
    public void testFirstName() {
    	
        String name = "jack";
        assertNull(user.getFirstName());
        user.setFirstName(name);
        assertEquals(name, user.getFirstName());
    }
    
    public void testEmail() {
    	
        String email = "jack@yahoo.com";
        assertNull(user.getEmailAddress());
        user.setEmailAddress(email);
        assertEquals(email, user.getEmailAddress());
    }
    
}
