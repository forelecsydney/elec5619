package au.usyd.elec5619.Controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.usyd.elec5619.Managers.*;

public class SimpleUserController implements Controller{

	private UserManager userManager;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        Map<String, Object> myModel = new HashMap<String, Object>();
        myModel.put("user", this.userManager.getUser("farhad"));

        return new ModelAndView("user", "model", myModel);
    }
		
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

}
