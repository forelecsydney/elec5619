package au.usyd.elec5619.Managers;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Request;
import au.usyd.elec5619.Classes.Consult;
import au.usyd.elec5619.Classes.Doctor;
public interface UserManager extends Serializable{

	public String getLoggedInUser();
	
	public User getUser(String username);

    public void addUser(User user);

    public void updateUser(User user);
    
    public void deleteUser(int id);
    
    public int getUserForLogin(String username,String password);
    
    public void logOutUser();
    
    public User returnUserObject();
    
    public int alreadyRequested(String doctorusername, String username);
    
    public void addRequest(Request request);
    
    public List<Request> getAllRequests(String username);
    
    public void deleteRequest(int id);
    
    public List<Consult> getAllConsults(String username);
    
    public void addConsult(Consult consult);
    
    public void deleteConsult(int id);
    
    
    
	public String getLoggedInDoctor();
	
	public List<Doctor> getDoctor(String username);
	
	public List<Doctor> getAllDoctors();

    public void addDoctor(Doctor doctor);

    public void updateDoctor(Doctor doctor);
    
    public void deleteDoctor(int id);
    
    public int getDoctorForLogin(String username,String password);
    
    public void logOutDoctor();
    
    public Doctor returnDoctorObject();
    
    public List<Request> getAllRequestsForDoctor(String doctorusername);
    
    public Request getRequest(String username, String doctorusername);
    
    public List<Meal> getMeals(String username);
    
    public void addMeal(Meal meal);
    
    public void deleteMeal(int id);
}