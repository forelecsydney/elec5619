<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
	<head>
		<title><fmt:message key="title" /></title>
	</head>
	<body>
		<div>
			<ul>
				<li class="active"><a href="profile.htm">Profile</a></li>
				<li class="active"><a href="logout.htm">LogOut</a></li>
			</ul>
		</div>
		<br/><br/>
		<h3>Meals</h3>
		<br/><br/>
		<c:forEach items="${model.meals}" var="meal">
		
			<c:out value="${meal.date}" /> ||
			<c:out value="${meal.description}" /> ||
			<c:out value="${meal.number}" />||
			<c:out value="${meal.photoName}" />
			<br/>
		</c:forEach>
	
		<!-- link to the increase price page -->
		<br>
		<!--  <a href="<c:url value="priceincrease.htm"/>">Increase Prices</a>  -->
		<br>
	</body>
</html>