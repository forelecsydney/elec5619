<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
	<head>
		<title><fmt:message key="title" /></title>
	</head>
	<body>
		<div>
			<ul>
				<li><a href="../home.htm">Home</a></li>
				<li><a href="register.htm">Register</a></li>
				<li class="active"><a href="login.htm">Login</a></li>
				<li class="active"><a href="meals.htm">Meals</a></li>
				<li class="active"><a href="logout.htm">LogOut</a></li>
			</ul>
		</div>

		<h3>Profile</h3>
		<c:forEach items="${model1.users}" var="usr">
		
			Username: <c:out value="${usr.userName}" /> <br/>
			First Name: <c:out value="${usr.firstName}" /> <br/>
			Last Name: <c:out value="${usr.lastName}" /> <br/>
			Date of Birth: <c:out value="${usr.dateOfBirth}" /> <br/>
			Email: <c:out value="${usr.emailAddress}" /> <br/>
			<a href="user/edit/${usr.id }">edit</a> <br/>
			<a href="user/delete/${usr.id }">delete</a> 
			<br/>
		</c:forEach>
	
		<!-- link to the increase price page -->
		<br>
		<!--  <a href="<c:url value="priceincrease.htm"/>">Increase Prices</a>  -->
		<br>
	</body>
</html>