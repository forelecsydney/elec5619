package ControllerTest;

import java.util.Locale;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Controllers.*;
import junit.framework.TestCase;

public class UserControllerTest extends TestCase {

    public void testView() throws Exception{
        UserController controller = new UserController();
        ModelAndView modelAndView = controller.viewAndConsult("userName");
        assertEquals("model", modelAndView.getViewName());

    }
	
}
