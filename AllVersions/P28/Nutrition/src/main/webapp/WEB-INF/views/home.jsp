<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta information -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!-- Title-->
<title>CAPLET |  Admin HTML Themes</title>
<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<!-- CSS Stylesheet-->
<link type="text/css" rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="assets/css/bootstrap/bootstrap-themes.css" />
<link type="text/css" rel="stylesheet" href="assets/css/style.css" />

</head>
<body>
<div id="wrap">
		<header id="header">
				<nav id="nav-top">
						<div class="container">
							<ul class="contact-top">
									<li><a href="#"><i class="fa fa-phone"></i> +66 123456789</a></li>
									<li><a href="#"><i class="fa fa-envelope"></i> info@example.com </a></li>
							</ul>
							<ul class="social-top pull-right">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-flickr"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
				</nav>
				<nav id="nav-middle">
					<nav class="navbar" role="navigation">
						<div class="container">
							<div class="navbar-header">
									<ul class="navbar-xs pull-right  visible-xs">
											<li><button class="btn btn-header-search" ><i class="fa fa-search"></i></button></li>
											<li><a href="#" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></a></li>
									</ul>
									<a class="navbar-brand" href="#">
										<img src="assets/img/logo.png">
									</a> 
							</div>
							<div class="collapse navbar-collapse" id="navbar-collapse">
								<ul class="nav navbar-nav navbar-right hidden-xs" id="navigation">
								
									<li class="active"><a href="index.html">Home</a></li>
	
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Register<i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu dropdown-menu-left  multi-level animated fast" data-effect="fadeInDown">
											<li><a href="register.html">User Register</a></li>
											<li><a href="doctorRegister.html">Doctor Register</a></li>
										</ul>
									</li>										
									
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Login<i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu dropdown-menu-left  multi-level animated fast" data-effect="fadeInDown">
											<li><a href="login.html">User Login</a></li>
											<li><a href="doctorLogin.html">Doctor Login</a></li>
										</ul>
									</li>
									
									<li><a href="contact.htm">Contact</a></li>
									
								</ul>
							</div>
						</div>
					</nav>
				</nav>
		</header>
		<!--
		/////////////////////////////////////////////////////////////////////////
		//////////     TOP SEARCH CONTENT     ///////
		//////////////////////////////////////////////////////////////////////
		-->
		<div class="widget-top-search">
			<span class="icon"><a href="#" class="close-header-search"><i class="fa fa-times"></i></a></span>
			<form id="top-search">
					<h2><strong>Quick</strong> Search</h2>
					<div class="input-group">
							<input  type="text" name="q" placeholder="Find something..." class="form-control" />
							<span class="input-group-btn">
							<button class="btn" type="button" title="With Sound"><i class="fa fa-microphone"></i></button>
							<button class="btn" type="button" title="Visual Keyboard"><i class="fa fa-keyboard-o"></i></button>
							<button class="btn" type="button" title="Advance Search"><i class="fa fa-th"></i></button>
							</span>
					</div>
			</form>
		</div>
		<!-- //widget-top-search-->
		<section id="header-space"></section>
		<section  id="slide" class="bg-gray-lighter">
					<div class="tp-banner-container">
						<div class="tp-banner" >
							<ul>
								<!-- SLIDE  -->
								<li data-transition="fade" data-slotamount="2" data-masterspeed="500" >
									<!-- MAIN IMAGE -->
									<img src="assets/photos_preview/slide/transparent.png" alt="slidebg1" class="bg-theme-inverse">
									<!-- LAYERS -->
									<div class="tp-caption mediumlarge_light_white_center customin customout start"
										data-x="center"
										data-hoffset="0"
										data-y="30"
										data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
										data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="500"
										data-easing="Back.easeInOut"
										data-endspeed="300">
									    <strong>EAT <br/> HEALTHY</strong>
									</div>
												
									<!-- LAYER NR. 1 -->
									<div class="tp-caption  lfb"
										data-x="center"
										data-y="140"
										data-speed="500"
										data-start="600"
										data-easing="Power4.easeOut"
										data-endspeed="300"
										data-endeasing="Power1.easeIn"
										data-captionhidden="off"
										style="z-index: 6"><img src="assets/img/theme-pics/food.png" alt="" class="img-responsive"> 
									</div>
								</li>
				
							</ul>
							<div class="tp-bannertimer tp-bottom"></div>
					</div>
			</div>
		</section>
		
		
		<section class="section">
				<div class="container">
						<div class="row">
								<div class="text-center">
										<h2> <strong>HTML5</strong> &amp; CSS3 </h2>
										<h3 class="sub-title animated" data-effect="fadeInRight">Premium admin and front end template</h3>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-12">
										<article class="boxIcon animated" data-effect="fadeInLeft"> 
											<a href="#">
												<span> 
													<img class="img-circle img-responsive" src="assets/img/team/team-corporate-1.jpg" alt=""> 
												</span>
												<h2>HTML5 &amp; CSS3</h2>
												<p>Built with modern technologies like HTML5 and CSS3, SEO optimised</p>
											</a> 
										</article>
								</div>
								<div class="col-md-4 col-sm-12">
										<article class="boxIcon animated" data-effect="fadeInUp">
											<a href="#">
												<span>
													<img class="img-circle img-responsive" src="assets/img/team/team-corporate-2.jpg" alt="">
													<!--<i class="fa fa-laptop"></i>-->
												</span>
												<h2>Clean design</h2>
												<p>Elegant layouts that help you organize your content in the best way</p>
											</a> 
										</article>
								</div>
								<div class="col-md-4 col-sm-12">
										<article class="boxIcon animated" data-effect="fadeInRight">
											<a href="#">
												<span>
													<img class="img-circle img-responsive" src="assets/img/team/team-corporate-3.jpg" alt="">
												</span>
												<h2>Responsive design</h2>
												<p>Compatible with various desktop, tablet, and mobile devices.</p>
											</a>
										</article>
								</div>
						</div>
				</div>
		</section>
		
		
		<section class="section bg-darkorange">
				<div class="container">
						<div class="row">
								<div class="col-md-6 animated" data-effect="fadeInDown">
										<h1 class="highlight-title"><strong>Easy</strong> to use</h1>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
												veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
												ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
												velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
												cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
												id est laborum. 
										</p>
								</div>
								<div class="col-md-6 text-center">
										<div class="img-stack" style="margin-top:0px; margin-bottom:-28px">
												<img src="assets/img/theme-pics/fl1.png" alt="" class="img-responsive img-bottom  animated" data-effect="slideInRight">
												<img src="assets/img/theme-pics/fl2.png" alt="" class="img-responsive img-bottom  bottom-over animated" data-effect="slideInRight" data-start="300">
												<img src="assets/img/theme-pics/fl3.png" alt="" class="img-responsive img-bottom  bottom-over animated" data-effect="fadeInUp" data-start="1200">
										</div>
								</div>
						</div>
				</div>
		</section>
		
		
		<section class="section bg-inverse">
				<div class="container">
						<div class="row">
								<div class="col-md-6 animated" data-effect="pulse">
										<h3> <strong> CAPLET ADMIN AND FRONT END</strong><br>
												is perfect for clean presentation of your business. </h3>
								</div>
								<div class="col-md-6 text-center"> <a class="btn btn-lg btn-theme-inverse animated" data-effect="fadeInRight" title="" href="http://themeforest.net/item/caplet-admin-responsive-html-theme-/6537086?ref=zicedemo" target="blank"> PURCHASE NOW  </a> </div>
						</div>
				</div>
		</section>
			
		
		<section class="section">
				<div class="container">
					<div class="col-md-12 text-center animated" data-effect="fadeInUp">
							<h2>Our <strong>services</strong></h2>
							<h3 class="sub-title">We do a lot of things, we put them inside squared boxes</h3>
					</div>
					<div class="col-md-6 text-center">
						<div class="img-stack">
							<img src="assets/img/theme-pics/ser2.png" alt="" class="img-responsive animated" data-effect="bounceIn" data-start="300"> 
							<img src="assets/img/theme-pics/ser3.png" alt="" class="img-responsive animated" data-effect="bounceIn" data-start="600"> 
							<img src="assets/img/theme-pics/ser6.png" alt="" class="img-responsive animated" data-effect="shake" data-start="1200">  
						 	<img src="assets/img/theme-pics/ser1.png" alt="" class="img-responsive animated" data-effect="bounceIn"> 
							<img src="assets/img/theme-pics/ser4.png" alt="" class="img-responsive animated" data-effect="fadeInDown" data-start="900"> 
							<img src="assets/img/theme-pics/ser5.png" alt="" class="img-responsive animated" data-effect="bounce" data-start="700"> 
						 </div>
					</div>
					<div class="col-md-6">
									<br>
									<div class="service-item  animated" data-effect="fadeInDown">
									<span class="fa-stack fa-3x">
										<i class="fa fa-circle fa-stack-2x color-primary"></i>
										<i class="fa fa-cog fa-stack-1x color-white"></i>
									</span>
											<h4>Identity &amp; Branding</h4>
											<p>A beautiful product needs to be complemented with a great branding. Our
													design team will help you create it</p>
									</div>
									<div class="service-item  animated" data-effect="fadeInRight"> 
									<span class="fa-stack fa-3x">
										<i class="fa fa-circle  fa-stack-2x  color-purple"></i>
										<i class="fa fa-user fa-stack-1x color-white"></i>
									</span>
											<h4>User Experience</h4>
											<p>At the heart of everything we do lies a great user experience. We take
													pride in our interfaces and really think and test them through</p>
									</div>
									<div class="service-item  animated" data-effect="fadeInLeft">
									<span class="fa-stack fa-3x">
										<i class="fa fa-circle  fa-stack-2x color-darkorange"></i>
										<i class="fa fa-camera fa-stack-1x color-white"></i>
									</span>
											<h4>Photography</h4>
											<p>A beautiful product needs to be complemented with a great branding. Our
													design team will help you create it</p>
									</div>
									<div class="service-item  animated" data-effect="fadeInUp">
									<span class="fa-stack fa-3x">
										<i class="fa fa-circle  fa-stack-2x color-theme"></i>
										<i class="fa fa-file fa-stack-1x color-white"></i>
									</span>
											<h4>HTML5 / CSS3 </h4>
											<p>A beautiful product needs to be complemented with a great branding. Our
													design team will help you create it</p>
									</div>
					</div>
				</div>
		</section>
					
		
		<section class="section bg-purple overflow bg-overlay">
				<div class="container">
						<div class="row">
								<div class="col-md-6  col-md-offset-3 topindex">
								<h2 class="text-center"><strong>SUBSCRIBE</strong> to newsletter</h2>
								<h3 class="sub-title text-center">We've Completed For Our Amazing Clients.</h3><br>
										<div class="input-group">
												<input  type="text" name="email" placeholder="Enter your e-mail" class="form-control input-lg bg-white" />
												<span class="input-group-btn">
												<button class="btn btn-lg btn-danger" type="button">Subscribe</button>
												</span>
										</div>		
								</div>
								<div class="col-md-12">
									<div class="img-stack" style="display:block">
										<img alt="" src="assets/img/theme-pics/st1.png" class="img-responsive animated" data-effect="fadeInUp">
										<img alt="" src="assets/img/theme-pics/st2.png" class="img-responsive img-bottom animated" data-effect="fadeInUp" data-start="300">
										<img alt="" src="assets/img/theme-pics/st3.png" class="img-responsive img-bottom animated" data-effect="fadeInUp" data-start="1200">
										<img alt="" src="assets/img/theme-pics/st4.png" class="img-responsive img-bottom animated" data-effect="fadeInUp" data-start="900">
										<img alt="" src="assets/img/theme-pics/st5.png" class="img-responsive img-bottom animated" data-effect="fadeInUp" data-start="600">
									</div>
								</div>
						</div>
				</div>
		</section>
		
		
		<section class="section">
				<div class="container">
					<div class="col-md-12 text-center">
							<h2 class="highlight-title animated fast" data-effect="fadeInUp">CAP<strong>LET</strong>  Work</h2>
							<h3 class="sub-title animated fast" data-effect="fadeInDown">We've Completed For Our Amazing Clients.</h3>
					</div>
				</div>
		</section>
		

		<section  id="portfolio" class="mosaic mosaic-5 clearfix">
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/10.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Photo</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/2.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Full width image</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/9.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Video Vimeo</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/1.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Gallery</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/8.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Youtube Video</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/5.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Photo</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/7.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Photo</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipisicing
														elit, sed do eiusmod tempor incididunt ut labore et dolore magna
														aliqua.</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/3.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Photo</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/4.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Photo</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
				<article>
						<figure> <img alt="" src="assets/photos_preview/portfolio/6.jpg" class="img-responsive">
								<figcaption>
										<div class="info">
												<h3>Full width image</h3>
												<p class="hidden-xs">Lorem ipsum dolor sit amet</p>
										</div>
										<div class="link">
											<a href="assets/photos_preview/portfolio/preview.jpg"  rel="preview"><i class="fa fa-plus"></i></a>
										</div>
								</figcaption>
						</figure>
				</article>
		</section>
		
		
		<section class="section bg-theme-inverse">
				<div id="owl-demo" class="owl-carousel carousel-white">
						<div>
							<div class="col-md-6   col-md-offset-3">
									<blockquote class="text-center"> Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. </blockquote>
									<p class="text-center">Zicedemo - <em> Web designer , Programme Director</em></p>
							</div>
						</div>
						<div>
							<div class="col-md-6   col-md-offset-3">
									<h3 class="blockquote text-center"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu, ac tincidunt mauris lacus sed leo.</h3>
									<p class="text-center">Caroline Hennigan, Programme Director, Broadway</p>
							</div>
						</div>
						<div>
							<div class="col-md-6   col-md-offset-3">
									<blockquote class="text-center"> Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. Imagination is more important than knowledge. Knowledge is limited. Imagination encircles the world. </blockquote>
									<p class="text-center">Caroline Hennigan, Programme Director, Broadway</p>
							</div>
						</div>
				</div>
		</section>
		
		
		<footer id="footer">
				<section class="section top">
						<div class="container">
								<div class="row">
										<div class="col-md-4">
												<div class="widget">
														<h4>About us</h4>
														<p>Lorem ipsum dolor sit amet, Maecenas sed diam eget risus varius blandit sit amet non magna. Cras mattis consectetur purus sit amet. Lorem ipsum dolor sit amet. Maecenas sed diam eget risus varius blandit sit amet non magna. Mattis purus sit amet fermentum.
														</p>
												</div>
										</div>
										<div class="col-md-4">
												<h4>Tags</h4>
												<div class="tagcloud">
														<a href="#">Features</a>
														<a href="#">Inspiration</a>
														<a href="#">Showcase</a>
														<a href="#">Graphic Design</a>
														<a href="#">Illustration</a>
														<a href="#">Design</a>
														<a href="#">Web Design</a>
														<a href="#">Video</a>
														<a href="#">ART</a>
														<a href="#">New Work</a>
												</div>
										</div>
										<div class="col-md-4">
												<div class="widget">
														<h4>Contact Info</h4>
														<div class="contacts">
																<div class="contacts-item"> 
																		<i class="fa fa-map-marker"></i> 
																		<span class="contacts-item-value">194/4  Amphitheatre Parkway, Mountain View, <br>CA 94043, United States</span> 
																</div>
																<div class="contacts-item">
																		<i class="fa fa-phone"></i>
																		<span class="contacts-item-value">+123 456 789</span>
																</div>
																<div class="contacts-item">
																		<i class="fa fa-envelope-o"></i>
																		<span class="contacts-item-value"><a href="mailto:info@example.com">info@example.com</a></span>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</section>
				<section class="section bottom">
						<div class="container">
								<div class="row">
										<div class="col-md-6">
												<div class="copyright">&copy; 2014 All rights reserved. <a href="http://themeforest.net/user/zicedemo/">Capet Theme by Zicedemo</a></div>
										</div>
										<div class="col-md-6">
												<ul class="footer-link pull-right">
														<li><a href="#">Privacy policy</a></li>
														<li><a href="#">Terms &amp; Conditions</a></li>
												</ul>
										</div>
								</div>
						</div>
				</section>
		</footer>
</div>

<!--
////////////////////////////////////////////////////////////////////////
//////////     JAVASCRIPT  LIBRARY     //////////
/////////////////////////////////////////////////////////////////////
-->
		
<!-- Jquery Library -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- Modernizr Library For HTML5 And CSS3 -->
<script type="text/javascript" src="assets/js/modernizr/modernizr.js"></script>
<!-- Owl Carousel  -->
<script type="text/javascript" src="assets/plugins/owl-carousel/owl.carousel.js"></script>
<link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
<!-- Revolution Slide -->
<script type="text/javascript" src="assets/plugins/slide/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="assets/plugins/slide/js/jquery.themepunch.revolution.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/plugins/slide/css/settings.css" media="screen" />
<!-- Select Nav -->
<script type="text/javascript" src="assets/plugins/selectnav/selectnav.min.js"></script>
<!-- Fancybox plugin -->
<script type="text/javascript" src="assets/plugins/fancybox/jquery.fancybox.js"></script>	
<link href="assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" />
<!-- Library Themes Customize-->
<script type="text/javascript" src="assets/js/caplet.custom.js"></script>
<script type="text/javascript">
var revapi;
$(document).ready(function() {
	   revapi = $('.tp-banner').revolution({
			delay:9000,
			startwidth:1170,
			startheight:500,
			hideThumbs:10,
			lazyLoad:"on",
			onHoverStop:"off",
			fullWidth:"on",
			forceFullWidth:"on"
		});
});	//ready
</script>
</body>
</html>