package com.farhad.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.farhad.model.User;
import com.farhad.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User save(User user1) {
		return userRepository.save(user1);
	}

	public boolean findByLogin(String userName, String password) {	
		User user1 = userRepository.findByUserName(userName);
		
		if(user1 != null && user1.getPassword().equals(password)) {
			return true;
		} 
		
		return false;		
	}

	public boolean findByUserName(String userName) {
		User user1 = userRepository.findByUserName(userName);
		
		if(user1 != null) {
			return true;
		}
		
		return false;
	}

}
