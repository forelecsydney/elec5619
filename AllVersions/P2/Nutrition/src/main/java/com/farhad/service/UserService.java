package com.farhad.service;

import com.farhad.model.User;

public interface UserService {
	User save(User user1);
	boolean findByLogin(String userName, String password);
	boolean findByUserName(String userName);
}
