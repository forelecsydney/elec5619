						package au.usyd.elec5619.Managers;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Doctor;
import au.usyd.elec5619.Classes.Request;
import au.usyd.elec5619.Classes.Consult;

@Service(value="userManager")
@Transactional
public class DatabaseUserManager implements UserManager {
	
	public static int loggedin = 0;
	
	public static int loggedinDoctor = 0;
	
	public static String loggedin_User = "";
	
	public static String loggedin_Doctor = "";

	
	User userObject = new User();
	
	Doctor doctorObject = new Doctor();
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf1) {
		this.sessionFactory = sf1;
	}
	
	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public void updateUser(User user) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}
	
	@Override
	public void deleteUser(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		currentSession.delete(user);
	}

	@Override
	public void addDoctor(Doctor doctor) {
		this.sessionFactory.getCurrentSession().save(doctor);
	}	
	
	@Override
	public void updateDoctor(Doctor doctor) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(doctor);
	}
	
	@Override
	public void deleteDoctor(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Doctor doctor = (Doctor) currentSession.get(Doctor.class, id);
		currentSession.delete(doctor);
	}
	
	
	@Override
	public void addMeal(Meal meal) {
		this.sessionFactory.getCurrentSession().save(meal);
	}

	@Override
	public void deleteMeal(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Meal meal = (Meal) currentSession.get(Meal.class, id);
		currentSession.delete(meal);
	}

	@Override
	public void addRequest(Request request) {
		this.sessionFactory.getCurrentSession().save(request);
	}
	
	@Override
	public void deleteRequest(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Request request = (Request) currentSession.get(Request.class, id);
		currentSession.delete(request);
	}

	@Override
	public void addConsult(Consult consult) {
		this.sessionFactory.getCurrentSession().save(consult);
	}
	
	@Override
	public void deleteConsult(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Consult consult = (Consult) currentSession.get(Consult.class, id);
		currentSession.delete(consult);
	}
	
	@Override
	public User getUser(String username) {
		
		String hql = "FROM User where username=:username";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("username", username);
		User user = (User) query.uniqueResult();
		return user;
		
	}
	
	public int getUserForLogin(String username,String password) {
		
		String hql = "from User where username=:username and password=:password";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("username", username);
        query.setString("password", password);
        User result = (User) query.uniqueResult();
        
        if(result != null)
        {
        	loggedin = 1;
        	loggedin_User = username;
        	userObject = result;
        }		
        
        return loggedin;
	}

	public String getLoggedInUser()
	{
		return loggedin_User;
	}
	
	public User returnUserObject()
	{
		return userObject;
	}
	
	public void logOutUser()
	{
		loggedin = 0;
	}
	
	@Override
	public List<Doctor> getDoctor(String doctorusername) {
		
		String hql = "FROM Doctor where doctorusername=:doctorusername";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("doctorusername", doctorusername);
		List<Doctor> Doctor = query.list();
		return Doctor;
	}
	
	public List<Doctor> getAllDoctors() {
		
		String hql = "FROM Doctor where approved=:app";
		//String hql = "FROM Doctor";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("app", "0"); // Make it 1 later
		List<Doctor> doctors = query.list();
		return doctors;
	}
	
	public int alreadyRequested(String doctorusername, String username) {
		
		String hql = "from Request where doctorusername=:doctorusername and username=:username";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("doctorusername", doctorusername);
        query.setString("username", username);
        Request result = (Request) query.uniqueResult();
        
        if(result != null)
        {
        	return 1;
        }
        else 
        {
        	return 0;
        }
        
	}
	
	public Request getRequest(String username, String doctorusername) {
		
		String hql = "FROM Request  where username=:username and doctorusername=:doctorusername";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
        query.setString("username", username);
        query.setString("doctorusername", doctorusername);
		Request request = (Request) query.uniqueResult();
		return request;
	}
	
	
	public List<Request> getAllRequests(String username) {
		
		String hql = "FROM Request  where username=:username";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
        query.setString("username", username);
		List<Request> requests = query.list();
		return requests;
	}
	
	public List<Request> getAllRequestsForDoctor(String doctorusername) {
		
		String hql = "FROM Request  where doctorusername=:doctorusername";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
        query.setString("doctorusername", doctorusername);
		List<Request> doctorRequests = query.list();
		return doctorRequests;
	}
	
	public int getDoctorForLogin(String doctorusername,String password) {
		
		String hql = "from Doctor where doctorusername=:doctorusername and password=:password and approved=:approved";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setString("doctorusername", doctorusername);
        query.setString("password", password);
        query.setString("approved", "0"); // Make it 1 later
        Doctor result = (Doctor) query.uniqueResult();
        
        if(result != null)
        {
        	loggedinDoctor = 1;
        	loggedin_Doctor = doctorusername;
        	doctorObject = result;
        }		
        
        return loggedinDoctor;
	}
	
	public String getLoggedInDoctor()
	{
		return loggedin_Doctor;
	}
	
	public Doctor returnDoctorObject()
	{
		return doctorObject;
	}
	
	public void logOutDoctor()
	{
		loggedinDoctor = 0;
	}
	
	
	public List<Consult> getAllConsults(String username) {
		
		String hql = "FROM Consult  where username=:username ";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
        query.setString("username", username);
		List<Consult> consults = query.list();
		return consults;
	}
	
	
	public List<Meal> getMeals(String username) {
		
		String hql = "FROM Meal where username=:username";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Meal> meals = query.list();
		return meals;
		// ask how to get a single string as the result of query
	}
	
	public void deleteMeal(String id) {
		
		String hql = "DELETE FROM Meal where id=:mealId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString("mealId", id);
		Meal meal = (Meal)query.uniqueResult();
	}


}
