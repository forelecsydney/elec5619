package au.usyd.elec5619.Managers;

import au.usyd.elec5619.Managers.*;

import java.util.List;

import au.usyd.elec5619.Classes.*;

public class SimpleUserManager implements UserManager {

	
	private User user;
	
	
	
	
	@Override
	public String getLoggedInUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(String username) {
		return user;
	}

	@Override
	public void addUser(User user) {
		this.user = user;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getUserForLogin(String username, String password) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void logOutUser() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User returnUserObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int alreadyRequested(String doctorusername, String username) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void addRequest(Request request) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Request> getAllRequests(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteRequest(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Consult> getAllConsults(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addConsult(Consult consult) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteConsult(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getLoggedInDoctor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Doctor> getDoctor(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Doctor> getAllDoctors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addDoctor(Doctor doctor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateDoctor(Doctor doctor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteDoctor(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getDoctorForLogin(String username, String password) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void logOutDoctor() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Doctor returnDoctorObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Request> getAllRequestsForDoctor(String doctorusername) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Request getRequest(String username, String doctorusername) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Meal> getMeals(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addMeal(Meal meal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteMeal(int id) {
		// TODO Auto-generated method stub
		
	}

}
