<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta information -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!-- Title-->
<title>CAPLET |  Admin HTML Themes</title>
<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<!-- CSS Stylesheet-->
<link type="text/css" rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="assets/css/bootstrap/bootstrap-themes.css" />
<link type="text/css" rel="stylesheet" href="assets/css/style.css" />

</head>
<body>
<div id="wrap">
		<header id="header">
				<nav id="nav-top">
						<div class="container">
							<ul class="contact-top">
									<li><a href="#"><i class="fa fa-phone"></i> +66 123456789</a></li>
									<li><a href="#"><i class="fa fa-envelope"></i> info@example.com </a></li>
							</ul>
							<ul class="social-top pull-right">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-flickr"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
				</nav>
				<nav id="nav-middle">
					<nav class="navbar" role="navigation">
						<div class="container">
							<div class="navbar-header">
									<ul class="navbar-xs pull-right  visible-xs">
											<li><button class="btn btn-header-search" ><i class="fa fa-search"></i></button></li>
											<li><a href="#" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></a></li>
									</ul>
									<a class="navbar-brand" href="#">
										<img style="width:100px; height:100px;" src="assets/img/LG.png">
									</a> 
							</div>
							<div class="collapse navbar-collapse" id="navbar-collapse">
								<ul class="nav navbar-nav navbar-right hidden-xs" id="navigation">
								
									<li class="active"><a href="index.html">Home</a></li>
	
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Register<i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu dropdown-menu-left  multi-level animated fast" data-effect="fadeInDown">
											<li><a href="register.html">User Register</a></li>
											<li><a href="doctorRegister.html">Doctor Register</a></li>
										</ul>
									</li>										
									
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Login<i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu dropdown-menu-left  multi-level animated fast" data-effect="fadeInDown">
											<li><a href="login.html">User Login</a></li>
											<li><a href="doctorLogin.html">Doctor Login</a></li>
										</ul>
									</li>
									
									<li><a href="contact.htm">Contact</a></li>
									
								</ul>
							</div>
						</div>
					</nav>
				</nav>
		</header>
		<!--
		/////////////////////////////////////////////////////////////////////////
		//////////     TOP SEARCH CONTENT     ///////
		//////////////////////////////////////////////////////////////////////
		-->
		<div class="widget-top-search">
			<span class="icon"><a href="#" class="close-header-search"><i class="fa fa-times"></i></a></span>
			<form id="top-search">
					<h2><strong>Quick</strong> Search</h2>
					<div class="input-group">
							<input  type="text" name="q" placeholder="Find something..." class="form-control" />
							<span class="input-group-btn">
							<button class="btn" type="button" title="With Sound"><i class="fa fa-microphone"></i></button>
							<button class="btn" type="button" title="Visual Keyboard"><i class="fa fa-keyboard-o"></i></button>
							<button class="btn" type="button" title="Advance Search"><i class="fa fa-th"></i></button>
							</span>
					</div>
			</form>
		</div>
		<!-- //widget-top-search-->
		<section id="header-space"></section>
		<section  id="slide" class="bg-gray-lighter">
					<div class="tp-banner-container">
						<div class="tp-banner" >
							<ul>
								<!-- SLIDE  -->
								<li data-transition="fade" data-slotamount="2" data-masterspeed="500" >
									<!-- MAIN IMAGE -->
									<img src="assets/photos_preview/slide/transparent.png" alt="slidebg1" class="bg-theme-inverse">
									<!-- LAYERS -->
									<div class="tp-caption mediumlarge_light_white_center customin customout start"
										data-x="center"
										data-hoffset="0"
										data-y="30"
										data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
										data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="500"
										data-easing="Back.easeInOut"
										data-endspeed="300">
									    <strong>EAT <br/> HEALTHY</strong>
									</div>
												
									<!-- LAYER NR. 1 -->
									<div class="tp-caption  lfb"
										data-x="center"
										data-y="140"
										data-speed="500"
										data-start="600"
										data-easing="Power4.easeOut"
										data-endspeed="300"
										data-endeasing="Power1.easeIn"
										data-captionhidden="off"
										style="z-index: 6"><img src="assets/img/theme-pics/food.jpeg" alt="" class="img-responsive" style="width:40%;height:40%;"> 
									</div>
								</li>
				
							</ul>
							<div class="tp-bannertimer tp-bottom"></div>
					</div>
			</div>
		</section>
		
		
		<section class="section">
				<div class="container">
						<div class="row">
								<div class="text-center">
										<h2> <strong>Nutrition Health</strong></h2>
										<h3 class="sub-title animated" data-effect="fadeInRight">We will help you to have a healthier diet ;)</h3>
								</div>
						</div>
						<div class="row">
								<div class="col-md-4 col-sm-12">
									<article class="boxIcon animated" data-effect="fadeInLeft"> 
										<a href="#">
											<img style="width:250px; height:250px;" src="assets/img/team/happy1.jpg" alt=""> 
											<br/>
											<br/>
											<h2>Upload Your Food</h2>
										</a> 
									</article>
								</div>
								<div class="col-md-4 col-sm-12">
										<article class="boxIcon animated" data-effect="fadeInUp">
											<a href="#">
													<img style="width:250px; height:250px;" src="assets/img/team/happy2.jpg" alt="">
													<br/>
													<br/>
												<h2>Request for Consult</h2>
											</a> 
										</article>
								</div>
								<div class="col-md-4 col-sm-12">
										<article class="boxIcon animated" data-effect="fadeInRight">
											<a href="#">
													<img style="width:250px; height:250px;" src="assets/img/team/happy3.jpg" alt="">
													<br/>
													<br/>
												<h2>Receive Consult</h2>
											</a>
										</article>
								</div>
						</div>
				</div>
		</section>
		
		<footer id="footer">
				<section class="section top">
						<div class="container">
								<div class="row">
										<div class="col-md-4">
												<div class="widget">
														<h4>About us</h4>
														<p>Lorem ipsum dolor sit amet, Maecenas sed diam eget risus varius blandit sit amet non magna. Cras mattis consectetur purus sit amet. Lorem ipsum dolor sit amet. Maecenas sed diam eget risus varius blandit sit amet non magna. Mattis purus sit amet fermentum.
														</p>
												</div>
										</div>

										<div class="col-md-4">
												<div class="widget">
														<h4>Contact Info</h4>
														<div class="contacts">
																<div class="contacts-item"> 
																		<i class="fa fa-map-marker"></i> 
																		<span class="contacts-item-value">194/4  Amphitheatre Parkway, Mountain View, <br>CA 94043, United States</span> 
																</div>
																<div class="contacts-item">
																		<i class="fa fa-phone"></i>
																		<span class="contacts-item-value">+123 456 789</span>
																</div>
																<div class="contacts-item">
																		<i class="fa fa-envelope-o"></i>
																		<span class="contacts-item-value"><a href="mailto:info@example.com">info@example.com</a></span>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</section>
				<section class="section bottom">
						<div class="container">
								<div class="row">
										<div class="col-md-6">
												<div class="copyright">&copy; 2014 All rights reserved. <a href="http://themeforest.net/user/zicedemo/">Capet Theme by Zicedemo</a></div>
										</div>
										<div class="col-md-6">
												<ul class="footer-link pull-right">
														<li><a href="#">Privacy policy</a></li>
														<li><a href="#">Terms &amp; Conditions</a></li>
												</ul>
										</div>
								</div>
						</div>
				</section>
		</footer>
</div>

<!--
////////////////////////////////////////////////////////////////////////
//////////     JAVASCRIPT  LIBRARY     //////////
/////////////////////////////////////////////////////////////////////
-->
		
<!-- Jquery Library -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- Modernizr Library For HTML5 And CSS3 -->
<script type="text/javascript" src="assets/js/modernizr/modernizr.js"></script>
<!-- Owl Carousel  -->
<script type="text/javascript" src="assets/plugins/owl-carousel/owl.carousel.js"></script>
<link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
<!-- Revolution Slide -->
<script type="text/javascript" src="assets/plugins/slide/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="assets/plugins/slide/js/jquery.themepunch.revolution.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/plugins/slide/css/settings.css" media="screen" />
<!-- Select Nav -->
<script type="text/javascript" src="assets/plugins/selectnav/selectnav.min.js"></script>
<!-- Fancybox plugin -->
<script type="text/javascript" src="assets/plugins/fancybox/jquery.fancybox.js"></script>	
<link href="assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" />
<!-- Library Themes Customize-->
<script type="text/javascript" src="assets/js/caplet.custom.js"></script>
<script type="text/javascript">
var revapi;
$(document).ready(function() {
	   revapi = $('.tp-banner').revolution({
			delay:9000,
			startwidth:1170,
			startheight:500,
			hideThumbs:10,
			lazyLoad:"on",
			onHoverStop:"off",
			fullWidth:"on",
			forceFullWidth:"on"
		});
});	//ready
</script>
</body>
</html>