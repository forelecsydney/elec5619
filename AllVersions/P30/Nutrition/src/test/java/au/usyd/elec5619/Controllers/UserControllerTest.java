package au.usyd.elec5619.Controllers;

import org.springframework.web.servlet.ModelAndView;

import junit.framework.TestCase;

public class UserControllerTest extends TestCase {

    public void testView() throws Exception{
        UserController controller = new UserController();
        ModelAndView modelAndView = controller.viewAndConsult("userName");
        assertEquals("model", modelAndView.getViewName());

    }
	
}
