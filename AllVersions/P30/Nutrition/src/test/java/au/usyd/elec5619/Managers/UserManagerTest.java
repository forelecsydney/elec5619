package au.usyd.elec5619.Managers;

import junit.framework.TestCase;
import au.usyd.elec5619.Classes.User;


public class UserManagerTest extends TestCase{

	 private SimpleUserManager userManager;
	 private User user;
	 
	 protected void setUp() throws Exception {
		 
		 	userManager= new SimpleUserManager();
			user = new User();
			
			user.setUserName("jack123");;
			user.setPassword("Ramsey123");
			user.setFirstName("Gordon");
			user.setLastName("Ramsay");
			user.setDateOfBirth("10/09/1986");
			user.setEmailAddress("jack@yahoo.com");
			user.setIllness("none");
			user.setAge("45");
			user.setWeight("90");
			user.setHeight("180");
			
			userManager.addUser(user);
	    }
	 
	   public void GetNoUser() {
		   userManager= new SimpleUserManager();
	       assertNull(userManager.getUser(""));
	    }
	    
	    public void testGetProducts() {
	        User user = userManager.getUser("jack123");
	        assertNotNull(user);        
	        assertEquals("Gordon", userManager.getUser("jack123").getFirstName());
	        assertEquals("Ramsay", userManager.getUser("jack123").getLastName());
	        assertEquals("45", userManager.getUser("jack123").getAge());
	    }   
	 
}
