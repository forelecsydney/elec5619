package au.usyd.elec5619.Controllers;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Managers.UserManager;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController 
{
	private UserManager userManager;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/", method = RequestMethod.GET)
	
	public String home(Locale locale, Model model)
	{
		logger.info("Welcome home! The client locale is {}.", locale);
		return "home";
	}
	
	private static final Logger logger0 = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	
	public String home0(Locale locale, Model model)
	{
		logger.info("Welcome home! The client locale is {}.", locale);
		return "home";
	}
	
	private static final Logger logger4 = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	
	public String home4(Locale locale4, Model model4)
	{
		logger.info("Welcome home! The client locale is {}.", locale4);
		return "home";
	}
	
	private static final Logger logger1 = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "members/login", method = RequestMethod.GET)
	
	public String login(Locale locale1, Model model1)
	{
		logger.info("Welcome home! The client locale is {}.", locale1);
		return "login";
	}
	
	private static final Logger logger2 = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "members/register", method = RequestMethod.GET)
	
	public String signup(Locale locale2, Model model2)
	{
		logger.info("Welcome home! The client locale is {}.", locale2);
		return "register";
	}

	private static final Logger logger5 = LoggerFactory.getLogger(HomeController.class);
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	
	public String home5(Locale locale, Model model)
	{
		logger.info("Welcome home! The client locale is {}.", locale);
		return "health/HTML/front/index";
	}
	
}
