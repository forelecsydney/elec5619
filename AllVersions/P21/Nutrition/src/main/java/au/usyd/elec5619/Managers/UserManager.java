package au.usyd.elec5619.Managers;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Doctor;
public interface UserManager extends Serializable{

   // public void increasePrice(int percentage);
    
	public String getLoggedInUser();
	
	public List<User> getUser(String username);

    public List<Meal> getMeals(String username);
    
    public void addUser(User user);
    
    public void addMeal(Meal meal);
    
    public void addDoctor(Doctor doctor);
    
    public User getUserById(int id);
    
    public void updateUser(User user);
    
    public void deleteUser(int id);
    
    public int getUserForLogin(String username,String password);
    
    public int getDoctorForLogin(String username,String password);
    
    public void logIn();
    
    public void logOutUser();
    
    public User returnUserObject();
    
    public void deleteMeal(int id);
}