package au.usyd.elec5619.Controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.Classes.Meal;
import au.usyd.elec5619.Classes.Product;
import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Doctor;
import au.usyd.elec5619.Managers.UserManager;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);
	
	int loggedin = 0;
	
	int loggedinDoctor = 0;
	
	@Resource(name="userManager")
	private UserManager userManager;

	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String addUser(HttpServletRequest httpServletRequest) {
		
		User user = new User();
		user.setUserName(httpServletRequest.getParameter("username"));
		user.setPassword(httpServletRequest.getParameter("password"));
		user.setFirstName(httpServletRequest.getParameter("firstName"));
		user.setLastName(httpServletRequest.getParameter("lastName"));
		user.setDateOfBirth(httpServletRequest.getParameter("dateOfBirth"));
		user.setEmailAddress(httpServletRequest.getParameter("email"));
		user.setIllness(httpServletRequest.getParameter("illness"));
		user.setAge(httpServletRequest.getParameter("age"));
		user.setWeight(httpServletRequest.getParameter("weight"));
		user.setHeight(httpServletRequest.getParameter("height"));
		//user.setPrice(Double.valueOf(httpServletRequest.getParameter("price")));
		this.userManager.addUser(user);
		
		return "redirect:/login.htm";
	}
	
	@RequestMapping(value="/doctorRegister", method=RequestMethod.POST)
	public String addDoctor(HttpServletRequest httpServletRequest) {
		
		Doctor doctor = new Doctor();
		doctor.setDoctorUserName(httpServletRequest.getParameter("username"));
		doctor.setPassword(httpServletRequest.getParameter("password"));
		doctor.setFirstName(httpServletRequest.getParameter("firstName"));
		doctor.setLastName(httpServletRequest.getParameter("lastName"));
		doctor.setDateOfBirth(httpServletRequest.getParameter("dateOfBirth"));
		doctor.setEmailAddress(httpServletRequest.getParameter("email"));
		doctor.setAbout(httpServletRequest.getParameter("about"));
		
		this.userManager.addDoctor(doctor);
		
		return "redirect:/DoctorLogin.htm";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String logIn(HttpServletRequest httpServletRequest) {
		
		String s1 =  httpServletRequest.getParameter("username");
		String s2 =  httpServletRequest.getParameter("password");
		
		loggedin = this.userManager.getUserForLogin(s1, s2);

        if(loggedin == 1)
        {	
        	return "redirect:/meals.htm";
        }
        else
        {
        	return "redirect:/login.htm";
        }
		
	}
	
	@RequestMapping(value="/doctorLogin", method=RequestMethod.POST)
	public String DoctorlogIn(HttpServletRequest httpServletRequest) {
		
		String s1 =  httpServletRequest.getParameter("username");
		String s2 =  httpServletRequest.getParameter("password");
		
		loggedinDoctor = this.userManager.getDoctorForLogin(s1, s2);

        if(loggedinDoctor == 1)
        {	
        	return "redirect:/request.htm";
        }
        else
        {
        	return "redirect:/doctorLogin.htm";
        }
		
	}
	
	
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)	
	public ModelAndView profile(Locale locale3, Model model3)
	{
			String currentUser = this.userManager.getLoggedInUser();
		    List<User> profile =  this.userManager.getUser(currentUser);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("profile", profile);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("profile", "model1", myModel1);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel1);
	        }
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)	
	public String updateUser(HttpServletRequest httpServletRequest)
	{
		
			User object = this.userManager.returnUserObject();
			
			object.setPassword(httpServletRequest.getParameter("password"));
			object.setFirstName(httpServletRequest.getParameter("firstName"));
			object.setLastName(httpServletRequest.getParameter("lastName"));
			object.setDateOfBirth(httpServletRequest.getParameter("dateOfBirth"));
			object.setEmailAddress(httpServletRequest.getParameter("email"));
			object.setIllness(httpServletRequest.getParameter("illness"));
			object.setAge(httpServletRequest.getParameter("age"));
			object.setWeight(httpServletRequest.getParameter("weight"));
			object.setHeight(httpServletRequest.getParameter("height"));
			
			
			this.userManager.updateUser(object);
		
			String currentUser = this.userManager.getLoggedInUser();
		    List<User> profile =  this.userManager.getUser(currentUser);
	        Map<String, Object> myModel1 = new HashMap<String, Object>();
	        myModel1.put("profile", profile);
	        
	        return "redirect:/profile.htm";
	}
		
	
	@RequestMapping(value = "/meals", method = RequestMethod.POST)

	String uploadFileHandler(@RequestParam("number") String number, @RequestParam("date") String date, 
			@RequestParam("description") String description, @RequestParam("file") MultipartFile file) 
	{
			
		Random rand = new Random();
		int  n = rand.nextInt(1000000) + 1;
		
		String photoNumber = String.valueOf(n);
		
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = "D:/D/Spring/Images/";
				File dir = new File(rootPath + File.separator + this.userManager.getLoggedInUser());
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + photoNumber + ".jpg");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());
				
				
				Meal meal = new Meal();
				
				meal.setUserName(this.userManager.getLoggedInUser());
				meal.setDate(date);
				meal.setNumber(number);
				meal.setDescription(description);
				meal.setPhotoName(photoNumber);
				
				this.userManager.addMeal(meal);
				
				return "redirect:/meals.htm";
			} 
			catch (Exception e) 
			{
				return "redirect:/meals.htm";
			}
		} 	
		
		else 
		{
			
			Meal meal = new Meal();
			
			meal.setUserName(this.userManager.getLoggedInUser());
			meal.setDate(date);
			meal.setNumber(number);
			meal.setDescription(description);
			meal.setPhotoName(number+date);
			
			this.userManager.addMeal(meal);
			
			return "redirect:/meals.htm";
		}
	}
	
	
	@RequestMapping(value = "/meals", method = RequestMethod.GET)	
	public ModelAndView meals(Locale locale, Model model)
	{
			
			String currentUser = this.userManager.getLoggedInUser();
		    List<Meal> meals =  this.userManager.getMeals(currentUser);
	        Map<String, Object> myModel = new HashMap<String, Object>();
	        myModel.put("meals", meals);
	        
	        if(loggedin == 1)
	        {	
	        	return new ModelAndView("meals", "model", myModel);
	        }
	        else
	        {
	        	return new ModelAndView("", "", myModel);
	        }
	}

	
	
	@RequestMapping(value="/meals/{id}", method=RequestMethod.GET)
	public String deleteMeal(@PathVariable("id") int id) {
		
		this.userManager.deleteMeal(id);
		
		return "redirect:/meals.htm";
	}
	

	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)	
	public String logout(Locale locale4, Model model4)
	{
			this.userManager.logOutUser();
			loggedin = 0;
	        return "redirect:/login";

	}
	
	
	
	/*
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String editProduct(@PathVariable("id") int id, Model uiModel) {
		
		User user = this.userManager.getUserById(id);
		uiModel.addAttribute("user", user);
		
		return "edit";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editProduct(@Valid User user) {
		
		this.userManager.updateUser(user);
		System.out.println(user.getId());
		
		return "redirect:/hello.htm";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String deleteUSer(@PathVariable("id") int id) {
		
		this.userManager.deleteUser(id);
		
		return "redirect:/hello.htm";
	}
	*/
}
