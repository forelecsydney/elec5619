package ClassTest;

import au.usyd.elec5619.Classes.User;
import au.usyd.elec5619.Classes.Meal;

import junit.framework.TestCase;

public class UserTest extends TestCase {

    private User user;
    private Meal meal;
    
    protected void setUp() throws Exception {
        user = new User();
    }
	
    public void testFirstName() {
    	
        String name = "jack";
        assertNull(user.getFirstName());
        user.setFirstName(name);
        assertEquals(name, user.getFirstName());
    }
    
    public void testEmail() {
    	
        String email = "jack@yahoo.com";
        assertNull(user.getEmailAddress());
        user.setEmailAddress(email);
        assertEquals(email, user.getEmailAddress());
    }
    
}
