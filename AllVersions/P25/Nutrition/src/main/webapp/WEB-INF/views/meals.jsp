<%@ include file="/WEB-INF/views/include.jsp"%>



<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta information -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!-- Title-->
<title>CAPLET |  Admin HTML Themes</title>
<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/members/1assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="1assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="1assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="1assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="1assets/ico/favicon.ico">
<!-- CSS Stylesheet-->
<link type="text/css" rel="stylesheet" href="1assets/css/bootstrap/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="1assets/css/bootstrap/bootstrap-themes.css" />
<link type="text/css" rel="stylesheet" href="1assets/css/style.css" />

<style>
#validate-wizard{
	width:330px;
	margin:auto;
	}
</style>

</head>
<body class="full-lg">
<div id="wrapper">
				<nav id="nav-middle">
						<nav class="navbar" role="navigation">
								<div class="container">
										<div class="navbar-header">
												<ul class="navbar-xs pull-right  visible-xs">
														<li><button class="btn btn-header-search" ><i class="fa fa-search"></i></button></li>
														<li><a href="#" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></a></li>
												</ul>
										</div>
										<div class="collapse navbar-collapse" id="navbar-collapse">
										<ul class="nav navbar-nav navbar-right hidden-xs" id="navigation">
												<li class="active"><a href="profile">Profile</a></li>
												<li><a href="doctors">Doctors</a></li>
												<li><a href="requests">Requests</a></li>
												<li><a href="consults">Consults</a></li>
												<li><a href="logout">Log Out</a></li>
										</ul>
										</div>
								</div>
								<!-- /.container -->
						</nav>
				</nav>
<div id="main">
		<div class="real-border">
				<div class="row">
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
						<div class="col-xs-1"></div>
				</div>
		</div>
		<div class="container">
				<div class="row">
						<div class="col-lg-12">
						
								<div class="account-wall">
										<section class="align-lg-center">										
										<h1 class="login-title"><span>Meals</span></h1>
										<br>
										</section>

										<br/><br/>

										<div class="panel-body">
											<div class="table-responsive">
												<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
													<thead>
														<tr style="background-color:white;">
															<th>Meal No</th>
															<th>Meal Date</th>
															<th>Meal Description</th>
															<th>Meal Photo</th>
															<th width="10%">Action</th>
														</tr>
													</thead>
														
														<c:forEach items="${model.meals}" var="meal">
															<tbody align="center">	
																<tr>
																	<td><c:out value="${meal.number}" /></td>
																	<td valign="middle"><c:out value="${meal.date}" /></td>
																	<td><c:out value="${meal.description}" /></td>
																	<td><img src="<c:out value="D:/D/Spring/Images/Foods/${meal.userName}/${meal.photoName}.jpg"/>" style="width:80px; height:50px;" alt="image" ></td>
																	<td>
																		<span class="tooltip-area">
																		<a href="meals/${meal.id}"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
																		</span>
																	</td>
																</tr>
															</tbody>
														</c:forEach>
												</table>
											</div>		
										</div>	
										<div class="account-wall">
											<div class="panel-body">
												<div class="table-responsive">
													<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
														<thead>
															<tr style="background-color:white;">
																<th>Meal No</th>
																<th>Meal Date</th>
																<th>Meal Description</th>
																<th>Meal Photo</th>
																<th width="10%">Action</th>
															</tr>
														</thead>
														<tbody align="center">	
	
															<form method="POST" action="meals" enctype="multipart/form-data">
																<tr style="background-color:pink;">
																	<td><input type="text" name="number"></td>
																	<td><input type="text" name="date"></td>
																	<td><input type="text" name="description"></td>
																	<td><input type="file" name="file"></td>
																	<td><input type="submit" value="Upload"></td>
																</tr>
															</form>					
																							
														</tbody>
													</table>
												</div>
										</div>
									</div>
										
										
										
								<!-- //account-wall-->
								
						</div>
						<!-- //col-sm-6 col-md-4 col-md-offset-4-->
				</div>
				<!-- //row-->
		</div>
		<!-- //container-->
		
</div>
<!-- //main-->

		
</div>
<!-- //wrapper-->


<!--
////////////////////////////////////////////////////////////////////////
//////////     JAVASCRIPT  LIBRARY     //////////
/////////////////////////////////////////////////////////////////////
-->
		
<!-- Jquery Library -->
<script type="text/javascript" src="1assets/js/jquery.min.js"></script>
<script type="text/javascript" src="1assets/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="1assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- Modernizr Library For HTML5 And CSS3 -->
<script type="text/javascript" src="1assets/js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="1assets/plugins/mmenu/jquery.mmenu.js"></script>
<!-- Holder Images -->
<script type="text/javascript" src="1assets/plugins/holder/holder.js"></script>
<!-- Form plugins -->
<script type="text/javascript" src="1assets/plugins/form/form.js"></script>
<!-- Datetime plugins -->
<script type="text/javascript" src="1assets/plugins/datetime/datetime.js"></script>
<!-- Library Chart-->
<script type="text/javascript" src="1assets/plugins/chart/chart.js"></script>
<!-- Library Themes Customize-->
<script type="text/javascript" src="1assets/js/caplet.custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
		   //Login animation to center 
			function toCenter(){
					var mainH=$("#main").outerHeight();
					var accountH=$(".account-wall").outerHeight();
					var marginT=(mainH-accountH)/2;
						   if(marginT>30){
							   $(".account-wall").css("margin-top",marginT-15);
							}else{
								$(".account-wall").css("margin-top",30);
							}
				}
				var toResize;
				$(window).resize(function(e) {
					clearTimeout(toResize);
					toResize = setTimeout(toCenter(), 500);
				});
				
			//Canvas Loading
			  var throbber = new Throbber({  size: 32, padding: 17,  strokewidth: 2.8,  lines: 12, rotationspeed: 0, fps: 15 });
			  throbber.appendTo(document.getElementById('canvas_loading'));
			  throbber.start();
			  	
			$('#validate-wizard').bootstrapWizard({
					tabClass:"nav-wizard",
					onNext: function(tab, navigation, index) {
									var content=$('#step'+index);
									if(typeof  content.attr("parsley-validate") != 'undefined'){
													var $valid = content.parsley( 'validate' );
													if(!$valid){
																	return false;
													}
									};
									
					// Set the name for the next tab
					$('#step3 h3').find("span").html($('#fullname').val());
					},
					onTabClick: function(tab, navigation, index) {
									$.notific8('Please click <strong>next button</strong> to wizard next step!! ',{ life:5000, theme:"danger" ,heading:" Wizard Tip :); "});
									return false;
					},
					onTabShow: function(tab, navigation, index) {
									tab.prevAll().addClass('completed');
									tab.nextAll().removeClass('completed');
									if(tab.hasClass("active")){
													tab.removeClass('completed');
									}
									var $total = navigation.find('li').length;
									var $current = index+1;
									var $percent = ($current/$total) * 100;
									$('#validate-wizard').find('.progress-bar').css({width:$percent+'%'});
									$('#validate-wizard').find('.wizard-status span').html($current+" / "+$total);
									
									toCenter();
									
									var main=$("#main");
									//scroll to top
									main.animate({
										scrollTop: 0
									}, 500);
									if($percent==100){
										setTimeout(function () { main.addClass("slideDown") }, 100);
										setTimeout(function () { main.removeClass("slideDown") }, 3000);
										setTimeout( "window.location.href='login.html'", 3500 );
									}
									
					}
			});


});
</script>
</body>
</html>